﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    class MenuSelect
    {
        public static int Up(int chosenId, int AmountOfMenuItems)
        {
            chosenId--;
            if (chosenId < 0)
                chosenId += AmountOfMenuItems;
            return chosenId;
        }
        public static int Down(int chosenId, int AmountOfMenuItems)
        {
            chosenId++;
            if (chosenId >= AmountOfMenuItems)
                chosenId -= AmountOfMenuItems;
            return chosenId;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace TimeTrees
{
    class AddPerson
    {
        /*public static void DateBirtsCheck(List<Person> people)
        {
            bool i = false;
            foreach (var timeEvent in people)
            {
                
                    DateTime date = timeEvent.Birthday;
                    DateTime datedeath = DateTime.Now;
                    var date1 = new DateTime(2022, 01, 01);
                if (DateTime.IsLeapYear(date.Year) && ((date1.Year - date.Year <= 22) || (datedeath.Year - date.Year <= 22)))
                {
                    i = true;
                }
                else
                {
                    i = false; Console.WriteLine("Ошибка! Введена неправильная дата ");
                }
                
            }
        }*/
        static Person AddParent()
        {
            string answer;
            do
            {
                answer = Console.ReadLine();
                if (answer == "нет") continue;
                else if (answer == "да")
                {
                    Person parent = FindMenu.FindPersonMenu();
                    return parent;
                }
                else
                {
                    Console.WriteLine("Введённый ответ не распознан, введите ответ ещё раз");
                }
            } while (answer != "нет");
            return null;
        }
        public static int NewIndex(Person person)
        {
            person.Id = DataReader.idUse.Last() + 1;
            return person.Id;
        }
        public static void AddDataPeople()
        {
            Person person = new Person();
            string PeopleFile = Path.Combine(Environment.CurrentDirectory, "people.csv");
            person.Id = NewIndex(person);
            Console.WriteLine("Введите имя: ");
            person.Name = Console.ReadLine();

            Console.WriteLine("Введите дату рождения:");
            Console.WriteLine("Примеры ввода дат: 2003-12-30 или 2021-01 или 1999");
            person.Birthday = DataReader.ParseDate(Console.ReadLine());

            Console.WriteLine("Введите дату смерти:");
            string death = Console.ReadLine();
            if (death == "") person.Death = null;
            else person.Death = DataReader.ParseDate(death);
            

            Console.WriteLine("Добавить мать? (да/нет)");
            person.Mother = AddParent();
            Console.Clear();
            Console.WriteLine("Добавить отца?(да/нет)");
            Console.ReadKey();
            person.Father = AddParent();
            
            DataWriter.WriteNewPerson(PeopleFile,person);
            /*try
            {

                using (StreamWriter sw = new StreamWriter(PeopleFile, true, System.Text.Encoding.Default))
                {

                    sw.Write("\n" + person.Id + ";" + person.Name + ";" + person.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) + ";" + person.Death + ";"+ person.Mother + ";"+ person.Father) ;
                }
                Console.WriteLine("Запись выполнена");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }*/


        }
    }
}

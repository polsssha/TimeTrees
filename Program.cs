﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace TimeTrees
{
    class Program
    {
        const int PeopleSearchId = 0;
        const int SearchEventId = 1;
        const int AddPeopleId = 2;
        const int AddEventId = 3;
        const int EditPeopleDataId = 4;
        const int DeltaDatesId = 5;
        const int LeapYearBornId = 6;
        const int AmountOfMainMenuItems = 7;

        static void Main(string[] args)
        {
            string timelineFile = Path.Combine(Environment.CurrentDirectory, "timeline.csv");
            string peopleFile = Path.Combine(Environment.CurrentDirectory, "people.csv");

            List<Person>  peopleData = DataReader.ReadDataPeople(peopleFile);
            List <TimelineEvent> timelineData = DataReader.ReadDataTimeline(timelineFile, peopleData);

            int chosenId = 0;
            CreateMenu(chosenId);

            do
            {
                Console.CursorVisible = false;
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                ConsoleHelper.CleanConsole();
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        chosenId = MenuSelect.Up(chosenId, AmountOfMainMenuItems);
                        CreateMenu(chosenId);
                        break;
                    case ConsoleKey.DownArrow:
                        chosenId = MenuSelect.Down(chosenId, AmountOfMainMenuItems);
                        CreateMenu(chosenId);
                        break;
                    case ConsoleKey.Enter:
                        DoAction(chosenId, timelineData, peopleData, peopleFile);
                        break;
                    default:
                        CreateMenu(chosenId);
                        break;
                }
            } while (true);

        }
        static void CreateMenu(int idChosenNumber)
        {
            List<MenuItem> menuItem = new List<MenuItem>()
            {
                new MenuItem{Id=PeopleSearchId, Text="Найти человека"},
                new MenuItem{Id=AddPeopleId, Text="Добавить человека"},
                new MenuItem{Id=AddEventId, Text="Добавить событие"},
                new MenuItem{Id=LeapYearBornId, Text="Вывести имена юных людей, родившихся в високосный год"},
                new MenuItem{Id=EditPeopleDataId, Text="Редактировать данные человека"},
                new MenuItem{Id=DeltaDatesId, Text="Вычислить разницу между максимальной и минимальной датами"},
                new MenuItem{Id=SearchEventId, Text="Найти событие"}
            };

            for (int i = 0; i < menuItem.Count; i++)
                if (idChosenNumber == menuItem[i].Id)
                    menuItem[i].IsChosen = true;
            DrawMenu(menuItem);
        }

        static void DrawMenu(List<MenuItem> menuItem)
        {
            Console.WriteLine("Главное меню");
            for (int i = 0; i < menuItem.Count; i++)
            {
                foreach (var item in menuItem)
                {
                    if (item.Id == i)
                    {
                        if (item.IsChosen)
                        {
                            Console.BackgroundColor = ConsoleColor.DarkBlue;
                        }
                        Console.WriteLine(item.Text, Console.BackgroundColor);
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                }
            }
        }

        static void DoAction(int numberOfAction, List <TimelineEvent> timelineData, List<Person> peopleData,string peopleFile)
        {
            Console.Clear();
            switch (numberOfAction)
            {
                case DeltaDatesId:
                    (int years, int months, int days) = DeltaProgram.DeltaMinAndMaxDate(timelineData);
                    Console.WriteLine($"Между макс и мин датами прошло: { years} лет, { months} месяцев и { days} дней");
                    break;
                case LeapYearBornId:
                    LogicLeapYear.WriteNeededNames(peopleData);
                    break;
                case AddEventId:
                    AddTimeline.AddDataTimeline(timelineData,peopleData);
                    //AddPerson.DateBirtsCheck(peopleData);
                    break;
                case AddPeopleId:
                    AddPerson.AddDataPeople();
                    //AddPerson.DateBirtsCheck(peopleData);
                    break;
                case PeopleSearchId:
                    FindMenu.FindPersonMenu();
                    break;
                case EditPeopleDataId:
                    PersonEditor.EditPersonData(peopleData, peopleFile);
                    break;
                case SearchEventId:
                    FindEvent.Search(timelineData);
                    break;
            }
            ReturnToMenuOrExit(numberOfAction);
        }

        static void ReturnToMenuOrExit(int choice)
        {
            Console.CursorVisible = true;
            Console.WriteLine("\nХотите продолжить?");
            Console.WriteLine("Введите да, чтобы вернуться в главное меню");
            Console.WriteLine("Введите нет, чтобы завершить работу программы");
            bool answerIsCorrect = false;
            do
            {
                string answer = Console.ReadLine();
                answer = answer.ToLower();
                if (answer == "да")
                {
                    Console.Clear();
                    CreateMenu(choice);
                    answerIsCorrect = true;
                }
                else if (answer == "нет")
                {
                    Environment.Exit(0);

                }
                else
                    Console.WriteLine("Введённый ответ не распознан");
            } while (!answerIsCorrect);
        }

        
    }
}




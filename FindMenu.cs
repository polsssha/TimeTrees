﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TimeTrees
{
    class FindMenu
    {
        public static Person FindPersonMenu()
        {
            string peopleFile = Path.Combine(Environment.CurrentDirectory, "people.csv");
            List<Person> people = DataReader.ReadDataPeople(peopleFile);
            List<Person> found = new List<Person>();
            string name = string.Empty;
            int selectedIndex = 0;
            Person selectedPerson = new Person();
            do
            {
                ConsoleHelper.CleanConsole();
                Console.WriteLine("ПОИСК ЛЮДЕЙ");
                Console.CursorVisible = true;
                Console.WriteLine($"Начните вводить имя: {name}");
                if (string.IsNullOrEmpty(name)) found = people;
                else found = FilterPeople(people, name);
                PrintPeople(found, selectedIndex);

                Console.SetCursorPosition($"Начните вводить имя: {name}".Length, 1);
                ConsoleKeyInfo keyInfo = Console.ReadKey(true);

                if (char.IsLetter(keyInfo.KeyChar))
                {
                    name += keyInfo.KeyChar;
                    selectedIndex = 0;
                }
                else if ((keyInfo.Key == ConsoleKey.Backspace) & (name != ""))
                {
                    name = name.Remove(name.Length - 1);
                }
                else if (keyInfo.Key == ConsoleKey.DownArrow)
                {
                    selectedIndex = MenuSelect.Down(selectedIndex, found.Count);
                }
                else if (keyInfo.Key == ConsoleKey.UpArrow)
                {
                    selectedIndex = MenuSelect.Up(selectedIndex, found.Count);
                }
                else if ((keyInfo.Key == ConsoleKey.Enter) & (selectedIndex != 0))
                {
                    selectedPerson = found[selectedIndex];
                    break;
                }
                else if (keyInfo.Key == ConsoleKey.Escape)
                {
                    break;
                }
            } while (true);
            return selectedPerson;
        }



        static void PrintPeople(List<Person> people, int selectedIndex)
        {
            for (var i = 0; i < people.Count; i++)
            {
                Person person = people[i];
                if (selectedIndex == i)
                {
                    Console.BackgroundColor = ConsoleColor.Magenta;
                }
                if (person.Death != null)
                {
                    Console.WriteLine($"{person.Id}\t" + $"{person.Name}\t" + $"{person.Birthday:d}\t" + $"{person.Death:d}");
                }
                else
                {
                    Console.WriteLine($"{person.Id}\t" + $"{person.Name}\t" + $"{person.Birthday:d}\t" + $"жив");
                }
                Console.BackgroundColor = ConsoleColor.Black;
            }
        }
        static List<Person> FilterPeople(List<Person> people, string name)
        {
            List<Person> result = new List<Person>();
            foreach (Person person in people)
            {
                if (person.Name.Contains(name))
                    result.Add(person);

            }
            return result;
        }
    }
}

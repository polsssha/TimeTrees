﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace TimeTrees
{
    class DataReader
    {
        private const int idIndex = 0;
        private const int nameIndex = 1;
        private const int birthdayIndex = 2;
        private const int deathIndex = 3;
        const int MotherIndex = 4;
        const int FatherIndex = 5;

        public static List<int> idUse = new List<int>();
        public static List<Person> ReadDataPeople(string path)
        {
            string[] data = File.ReadAllLines(path.ToString());
            List<Person> splitData = new List<Person>(data.Length);
            for (var i = 0; i < data.Length; i++)
            {
                Person person = new Person();
                var line = data[i];
                string[] parts = line.Split(';');
                person.Id = int.Parse(parts[idIndex]);
                person.Name = parts[nameIndex];
                person.Birthday = ParseDate(parts[birthdayIndex]);
                if (parts.Length == 4)
                {
                    person.Death = ParseDate(parts[deathIndex]);
                }
                else person.Death = null;
               


                //splitData[i] = person1;

                splitData.Add(person);
                idUse.Add(person.Id);
            }
            for (int i = 0; i < data.Length; i++)
            {
                string line = data[i];
                string[] separation = line.Split(";");

                if (separation.Length == 5)
                {

                    foreach (Person person1 in splitData)
                        if (person1.Id == int.Parse(separation[MotherIndex]))
                            splitData[i].Mother = person1;
                }
                
                if (separation.Length == 6)
                {
                    foreach (Person person1 in splitData)
                        if (person1.Id == int.Parse(separation[FatherIndex]))
                            splitData[i].Father = person1;
                }
                
            }

            /*for (int i = 0; i < data.Length; i++)
            {
                string line = data[i];
                string[] separation = line.Split(";");
                if (int.Parse(separation[MotherIndex]) != 0)
                {
                    foreach (Person person1 in splitData)
                        if (person1.Id == Int32.Parse(separation[MotherIndex]))
                            splitData[i].Mother = person1;
                }
                if (int.Parse(separation[FatherIndex]) != 0)
                {
                    foreach (Person person1 in splitData)
                        if (person1.Id == Int32.Parse(separation[FatherIndex]))
                            splitData[i].Father = person1;
                }
            }*/


            return splitData;
        }
        public static List <TimelineEvent> ReadDataTimeline(string path, List<Person> people)
        {
            string[] data = File.ReadAllLines(path.ToString());
            List <TimelineEvent> splitData = new List <TimelineEvent>(data.Length);
            for (var i = 0; i < data.Length; i++)
            {
                splitData.Add(new TimelineEvent());
                List<int> participants = new List<int>();

                var line = data[i];
                string[] parts = line.Split(';');
                splitData[i].Description = parts[1];
                splitData[i].Date = ParseDate(parts[0]);
                if (parts.Length > 2)
                {
                    string[] ids = parts[2].Split(' ');
                    if (ids[0] != "")
                        foreach (string id in ids)
                            foreach (Person person in people)
                                if (person.Id == int.Parse(id))
                                    participants.Add(person.Id);
                }
                    
                splitData[i].Participants = participants;
            }
            return splitData;
        }
        public static DateTime ParseDate(string value)
        {
            if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date))
            {
                if (!DateTime.TryParseExact(value, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    if (!DateTime.TryParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                    {
                        throw new Exception("WRONG FORMAT");
                    }
                }
            }

            return date;

        }

    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    class DeltaProgram
    {
        public static (int, int, int) DeltaMinAndMaxDate(List<TimelineEvent> timeline)
        {
            (DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timeline);
            return (maxDate.Year - minDate.Year,
                maxDate.Month - minDate.Month,
                maxDate.Day - minDate.Day);
        }
        public static (DateTime, DateTime) GetMinAndMaxDate(List<TimelineEvent> timeline)
        {
            DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;
            foreach (var timeEvent in timeline)
            {
                DateTime date = timeEvent.Date;
                if (date < minDate) minDate = date;
                if (date > maxDate) maxDate = date;
            }
            return (minDate, maxDate);
        }
    }
}

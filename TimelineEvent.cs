﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    class TimelineEvent
    {
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public List<int> Participants { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace TimeTrees
{
    class DataWriter
    {
        
        public static void  Write(List <Person> people, string PeopleFile )
        {
            File.WriteAllText(PeopleFile,"");
            foreach (Person person in people)
            {
                if (person.Death != null)
                {
                    if (person == people[0])
                    {
                        string personDeath = person.Death.ToString();
                        DateTime personDeath1 = DateTime.Parse(personDeath);
                        File.AppendAllText(PeopleFile, $"{person.Id};{person.Name};{person.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)};{personDeath1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}");
                    }

                    else
                    {
                        string personDeath = person.Death.ToString();
                        DateTime personDeath1 = DateTime.Parse(personDeath);
                        File.AppendAllText(PeopleFile, $"\n{person.Id};{person.Name};{person.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)};{personDeath1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}");
                    }

                }
                else if (person == people[0])
                {
                    File.AppendAllText(PeopleFile, $"{person.Id};{person.Name};{person.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}{person.Death}");
                }
                else File.AppendAllText(PeopleFile, $"\n{person.Id};{person.Name};{person.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}{person.Death}");
                if (person.Mother != null)
                    File.AppendAllText(PeopleFile, $";{person.Mother.Id}");
                if (person.Father != null)
                    File.AppendAllText(PeopleFile, $";{person.Father.Id}");
                //File.AppendAllText(peopleFile,$"{person1.Id};{person1.Name};{person1.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)};{person1.Death};{person1.Mother};{person1.Father}\n");
            }
            

        }
        public static void WriteNewEvent(List<TimelineEvent> timelineEvents,string timelineFile,TimelineEvent Event)
        {
            File.AppendAllText(timelineFile, $"\n{ Event.Date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)};{Event.Description }");
            if (Event.Participants != null)
                for (int i = 0; i < Event.Participants.Count; i++)
                {
                    File.AppendAllText(timelineFile, $";{Event.Participants[i]} ");
                }

            Console.WriteLine("Событие и участник(и) успешно добавлены");

        }
        public static void WriteNewPerson(string PeopleFile,Person person)
        {

             if (person.Death!= null)
             {
                string personDeath = person.Death.ToString();
                DateTime personDeath1 = DateTime.Parse(personDeath);     
                File.AppendAllText(PeopleFile,  $"\n{person.Id};{person.Name};{person.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)};{personDeath1.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}");
             }
             else File.AppendAllText(PeopleFile, $"{person.Id};{person.Name};{person.Birthday.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)}{person.Death}\n");
            if (person.Mother != null)
                File.AppendAllText(PeopleFile, $";{person.Mother.Id}");
            if (person.Father!= null)
                File.AppendAllText(PeopleFile, $";{person.Father.Id}");
            Console.Clear();
            Console.WriteLine("Запись выполнена");
             
        }
    }
}

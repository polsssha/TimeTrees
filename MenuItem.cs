﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    class MenuItem
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsChosen { get; set; }
    }
}

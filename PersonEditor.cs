﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrees
{
    class PersonEditor
    {
        public static void EditPersonData(List<Person> people, string peopleFile)
        {
            Console.WriteLine("Нажмите любую клавишу, чтобы начать выбор пользователя, данные которого хотите изменить");
            Console.ReadKey(true);
            Console.Clear();
            Person person = FindMenu.FindPersonMenu();
            if (person.Birthday == DateTime.MinValue) return;
            bool answerFit;
            bool editingFinished;
            WriteInstructionsInEditMenu();
            do
            {
                editingFinished = false;
                answerFit = true;
                char answer = Console.ReadKey(true).KeyChar;
                switch (answer)
                {
                    case '1':
                        EditPersonName(person);
                        break;
                    case '2':
                        EditPersonBirthDate(person);
                        break;
                    case '3':
                        EditPersonDeathDate(person);
                        break;
                    case '4':
                        Console.Clear();
                        person.Mother = FindMenu.FindPersonMenu();
                        break;
                    case '5':
                        Console.Clear();
                        person.Father = FindMenu.FindPersonMenu();
                        break;
                    default:
                        Console.WriteLine("Ответ нераспознан введите ещё раз");
                        answerFit = false;
                        break;
                }

                if (answerFit)
                {
                    Console.Clear();
                    Console.WriteLine("Нужно ли изменить что-то ещё? (да/нет)");
                    editingFinished = IsEditingFinished();
                    if (!editingFinished)
                    {
                        Console.Clear();
                        WriteInstructionsInEditMenu();
                    }
                }
            } while ((!answerFit) || !(editingFinished));
            people.RemoveAt(person.Id - 1);
            people.Insert(person.Id - 1, person);

            DataWriter.Write(people, peopleFile);
            Console.WriteLine("Редактирование выполнено успешно");

        }


        static void WriteInstructionsInEditMenu()
        {
            Console.Clear();
            Console.WriteLine("Меню изменения людей");
            Console.WriteLine("Что вы хотите изменить?");
            Console.WriteLine("1-ФИО, 2-дату рождения, 3-дату смерти, 4-мать, 5-отца");
            Console.WriteLine("Введите только цифру  1, или 2, или 3, или 4, или 5");
        }

        static void EditPersonName(Person person)
        {
            Console.WriteLine("Введите новое имя");
            string name = Console.ReadLine();
            person.Name = name;
            
        }

        static void EditPersonBirthDate(Person person)
        {
            Console.WriteLine("Введите новую дату рождения в формате год-месяц(если известен)-день(если известен)");
            string birthday = Console.ReadLine();
            DataReader.ParseDate(birthday);
            
            person.Birthday = DataReader.ParseDate(birthday);
        }

        static void EditPersonDeathDate(Person person)
        {
            Console.WriteLine("Введите новую дату смерти в формате yyyy-MM-dd,yyyy-MM,yyyy");
            Console.WriteLine("Если человек жив нажмите Enter");
            string deathDay = Console.ReadLine();
            if (deathDay == "") person.Death = null;
            else person.Death = DataReader.ParseDate(deathDay);
            
        }



        static bool IsEditingFinished()
        {
            bool answerIsCorrect = false;
            bool editingFinished = true;
            do
            {
                string secondAnswer = Console.ReadLine();
                secondAnswer = secondAnswer.ToLower();
                if (secondAnswer == "да")
                {
                    answerIsCorrect = true;
                    editingFinished = false;
                }
                else if (secondAnswer == "нет")
                {
                    answerIsCorrect = true;
                    editingFinished = true;
                }
                else
                    Console.WriteLine("Введённый ответ не распознан");
            } while (!answerIsCorrect);
            return editingFinished;
        }

    }
}

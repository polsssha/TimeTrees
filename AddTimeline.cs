﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace TimeTrees
{
    class AddTimeline
    {
      

        static List<int> AddParticipantsToEvent(DateTime dateOfEvent, List<Person> people)
        {
            List<int> participant = new List<int>();
            string answer = "";
            while (answer != "нет")
            {
                Console.Clear();
                Console.WriteLine("Хотите добавить участника события? (да/нет)");

                answer = Console.ReadLine();
                if (answer == "нет") continue;
                else if (answer == "да")
                {
                    Console.Clear();
                    Person newParticipant = FindMenu.FindPersonMenu();
                    participant.Add(newParticipant.Id);
                    

                }
                else Console.WriteLine("Ответ не распознан");
            }
            return participant;
        }

        public static void AddDataTimeline(List<TimelineEvent> timeline, List<Person> people)
        {
            TimelineEvent Event = new TimelineEvent();
            string timelineFile = Path.Combine(Environment.CurrentDirectory, "timeline.csv");

            Console.WriteLine("Добавление нового события");

            Console.WriteLine("Введите дату события в формате yyyy-MM-dd: ");
            //string date = " ";
            /*
            bool i = false;
            while (!i)
            {
                date = Console.ReadLine();
                int dateElementNum = date.Split('-').Length;
                switch (dateElementNum)
                {
                    case 1:
                        if (DateTime.TryParse(date + "-01", out var n)) { i = true; }
                        else { Console.WriteLine(" Попробуйте написать дату ещё раз "); }
                        break;
                    default:
                        if (DateTime.TryParse(date, out n))
                        {
                            date = String.Join("-", n.ToString("s", CultureInfo.CreateSpecificCulture("en-US")).Split('-'), 0, dateElementNum);
                            date = (date.Contains('T')) ? date.Split('T')[0] : date;
                            i = true;
                        }
                        else { Console.WriteLine(" Попробуйте написать дату ещё раз "); }
                        break;
                }
            }*/

            Event.Date = DataReader.ParseDate(Console.ReadLine());

            Console.WriteLine("Введите описание события: ");
            string description = Console.ReadLine();
            Event.Description = description;


            List<int> participant = AddParticipantsToEvent(Event.Date, people);
            Event.Participants = participant;
            timeline.Add(Event);
            DataWriter.WriteNewEvent(timeline, timelineFile, Event);


            /*try
            {

                using (StreamWriter sw = new StreamWriter(timelineFile, true, System.Text.Encoding.Default))
                {

                    sw.Write("\n" + Event.Date + ";" + Event.Description );
                    foreach(int n in Event.Participants)
                    {
                        if (Event.Participants != null)
                        {
                            for (var k = 0; k < Event.Participants.Count; k++)
                            {
                                sw.Write(";" + Event.Participants[k]);
                            }
                        }
                    }
                }
                Console.WriteLine("Запись выполнена");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }*/

        }
    }
}

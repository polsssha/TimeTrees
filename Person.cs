﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
namespace TimeTrees
{

    class Person
    {
        
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime? Death { get; set; }
        public Person Mother { get; set; }
        public Person Father { get; set; }


    }
}
